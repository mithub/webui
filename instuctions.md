# How to build
git clone repository

install node 12/14 with yarn

yarn install --frozen-lockfile

make changes

yarn run build:prod:aot

copy dist folder to location

# How to reset root password
run /etc/netcli and use the option "Reset Root Password"
