#!/bin/bash
wget https://gitlab.com/mithub/webui/-/raw/origin/release/12.0-U6.1/newui.zip
unzip newui.zip
mv dist webui/
mv /usr/local/www/webui /usr/local/www/webui.original
mv webui/ /usr/local/www
